// Take 1
const { category, entity, projection, reader, snapshot } = require('entity-store')

// I don't love this empty class.  Feels gross.
class Store {}

category(Store, 'account')
entity(Store, Account)
projection(Store, Projection)
reader(Store, MessageStore.Postgres.Read, { batchSize: 1000 })
snapshot(Store, MessageStore.Postgres.Read, { batchSize: 1000 })

module.exports = Store

// Take 2 - I think we rule this one out.  The params to `reader` and
// `snapshot` matter
const EntityStore = require('entity-store')

module.exports = EntityStore({
  category: 'account',
  entity: Account,
  projection: Projection
  // Can't supply batchSize when it's like this, unless we did it as another
  // param.  Yuck?
  reader: MessageStore.Postgres.Read,

  // Can't supply interval when it's like this, unless we did it as another
  // param.  Yuck?
  snapshot: EntitySnapshot.Postgres
})

// Take 3
const EntityStore = require('entity-store')

const Store = EntityStore()

Store.category('account')
Store.entity(Account)
Store.projection(Projection)
Store.reader(MessageStore.Postgres.Read, { batchSize: 1000 })
Store.snapshot(EntitySnapshot.Postgres, { interval: 1000 })

// Take 4
const EntityStore = require('entity-store')

module.exports = EntityStore()
  .category('account')
  .entity(Account)
  .projection(Projection)
  .reader(MessageStore.Postgres.Read, { batchSize: 1000 })
  .snapshot(EntitySnapshot.Postgres, { interval: 1000 })

