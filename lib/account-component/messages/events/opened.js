// Take 1
const Messaging = require('messaging')

const Opened = Messaging.Message()
  .type('Opened')
  .attribute('accountId', String, { defaultValue: '0', strict: false })
  .attribute('customerId', String)
  .attribute('time', String)
  .attribute('processedTime', String)

// Take 2
const Messaging = require('messaging')

class Opened {
  static attributes = {
    { name: 'accountId', type: String }
  }
}

module.exports = Messaging.Message(Opened)


// Cory's hot takes
// 1. Take one not so good (e.g. yargs does that)
// 2. Slightly better
