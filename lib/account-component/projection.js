// I like having the entity name supplied through a function call.  Could be
// useful in logs and such

// Take 1
const EntityProjection = require('entity-projection')

const { Events } = require('./messages')

module.exports = EntityProjection()
  .entityName('account')

  .apply(Events.Opened, opened => {
    account.id = opened.accountId
    account.customerId = opened.customerId

    openedTime = new Date(opened.time)

    account.openedTime = openedTime
  })

// Take 2
const EntityProjection = require('entity-projection')

const { Events } = require('./messages')

module.exports = EntityProjection('account')
  .apply(Events.Opened, opened => {
    account.id = opened.accountId
    account.customerId = opened.customerId

    openedTime = new Date(opened.time)

    account.openedTime = openedTime
  })

  .apply(Events.Deposited, deposited => {
    account.id = deposited.accountId

    amount = deposited.amount
    account.deposit(amount)

    account.sequence = deposited.sequence
  })

// Take 3 - Entity as a param on the `apply` functions
const EntityProjection = require('entity-projection')

const { Events } = require('./messages')

module.exports = EntityProjection()
  .apply(Events.Opened, (account, opened) => {
    account.id = opened.accountId
    account.customerId = opened.customerId

    openedTime = new Date(opened.time)

    account.openedTime = openedTime
  })

