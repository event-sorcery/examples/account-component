const Message = (function () {
  function attribute(name, type, options) {
    console.error('called attribute', name, type, options)
  }

  return function () {
    this.attribute = attribute
  }
})()

class Opened {
  static build (params) {
    const instance = new this()

    for (const [key, value] of Object.entries(params)) {
      console.error({ key, value })
      instance[key] = value
    }

    return instance
  }
}

Message.call(Opened)

const opened = Opened.build({ accountId: '123' })

console.error({ opened })
